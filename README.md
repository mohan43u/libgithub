### libgithub

GObject wrapper for github api. At present, it is capable of making Authorized call to https://api.github.com and returns json strings. Following functionalities implemented,

1. if gnome-online-accounts d-bus service have provider with ProviderName as "Github", it uses access_token from that provider
2. else, it acquires authorization manually and stores(and retrives) access_token in default-keyring (libsecret)
3. uses libsoup for making call to api.github.com

### plans

1. create GObjects for github api paths
2. integrate with autotools
3. make libgithub.so library
4. create bindings for libgithub.so (.gir, .typelib and .vapi)
5. create a dbus service daemon which uses libgithub library ("org.gnome.Github")

### compilation

```
$ cd src/
$ valac --pkg posix --pkg gio-unix-2.0 --pkg libsecret-1 --pkg libsoup-2.4 --pkg webkit2gtk-4.0 github.vala authorizer.vala secretstore.vala goaclient.vala simplesoup.vala channel.vala webkit.vala
```
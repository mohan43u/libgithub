/*
 * authorizer.vala - make oauth2 simple
 */

interface Github.OAuth2Authorizer : Object {
    public abstract Github.SimpleSoup soup { get; set construct; }
    public abstract Github.SecretStore store { get; set construct; }
    public abstract Github.GoaClient goaclient { get; set construct; }
    public abstract Github.Webkit webkit { get; set construct; }
    public abstract string client_id { get; set construct; }
    public abstract string client_secret { get; set construct; }
    public abstract string scope { get; set construct; }
    public abstract string authorize_uri { get; set construct; }
    public abstract string redirect_uri { get; set construct; }
    public abstract string token_uri { get; set construct; }
    public abstract string access_token { get; set construct; }

    public virtual Github.SimpleSoupRequest? get_token_gen_request(string? redirected_uri = null) throws Error {
        MatchInfo matchinfo;
        Regex regex = new Regex("code=([^&]*)");
        regex.match(redirected_uri, 0, out matchinfo);
        string code = matchinfo.fetch(1);
        if(code == null) return null;
        Github.SimpleSoupRequest request = new Github.SimpleSoupRequest();
        request.method = "POST";
        request.uri = this.token_uri;
        request.append_string("client_id=" + this.client_id);
        request.append_string("&client_secret=" + this.client_secret);
        request.append_string("&redirect_uri=" + Uri.escape_string(this.redirect_uri));
        request.append_string("&code=" + code);
        return request;
    }

    public virtual bool get_token_parse_response(Github.SimpleSoupResponse? response = null) throws Error {
        if(response == null) return false;
        string body = response.to_string();
        MatchInfo matchinfo = null;
        Regex regex = new Regex("access_token=([^&]*)");
        regex.match(body, 0, out matchinfo);
        this.access_token = matchinfo.fetch(1);
        if(this.access_token == null) return false;
        regex = new Regex("scope=([^&]*)");
        regex.match(body, 0, out matchinfo);
        this.scope = matchinfo.fetch(1);
        if(this.scope != null) this.scope = Uri.unescape_string(this.scope);
        return true;
    }

    public virtual async bool get_token(string? redirected_uri = null,
                                        Cancellable? cancellable = null) throws Error {
        Idle.add(this.get_token.callback);
        yield;
        Github.SimpleSoupRequest request = this.get_token_gen_request(redirected_uri);
        if(request == null) return false;
        return this.get_token_parse_response(yield this.soup.call(request, cancellable));
    }

    public virtual string gen_authorize_uri() throws Error{
        string authorize_uri = this.authorize_uri;
        authorize_uri += "?client_id=" + this.client_id;
        authorize_uri += "&redirect_uri=" + Uri.escape_string(this.redirect_uri);
        authorize_uri += "&scope=" + Uri.escape_string(this.scope);
        return authorize_uri;
    }

    public virtual async string authorize(string[]? args = null,
                                          Cancellable? cancellable = null) throws Error {
        Idle.add(this.authorize.callback);
        yield;
        this.webkit.authorize_uri = this.gen_authorize_uri();
        this.webkit.redirect_uri = this.redirect_uri;
        return yield this.webkit.show(args);
    }
    
    public virtual async bool init(string[]? args = null,
                                   Cancellable? cancellable = null) throws Error {
        Idle.add(this.init.callback);
        yield;
        this.access_token = yield this.store.retrive("access_token", null, cancellable);
        if(this.access_token == null) this.access_token = yield this.goaclient.init(cancellable);
        if(this.access_token != null) return true;
        string redirected_uri = yield this.authorize(args, cancellable);
        if(redirected_uri == null) return false;
        bool result = yield this.get_token(redirected_uri, cancellable);
        if(result == true) result = yield this.store.store("access_token", this.access_token, null, cancellable);
        return result;
    }

    public virtual async InputStream call_stream(Github.SimpleSoupRequest request,
                                                 Cancellable? cancellable = null,
                                                 out Github.SimpleSoupResponse response) throws Error {
        if(!("Authorization" in request.headers)) {
            request.append_header("Authorization", "Bearer " + this.access_token);
        }
        return yield this.soup.call_stream(request, cancellable, out response);
    }
    
    public virtual async Github.SimpleSoupResponse call(Github.SimpleSoupRequest request,
                                                        Cancellable? cancellable) throws Error {
        if(!("Authorization" in request.headers)) {
            request.append_header("Authorization", "Bearer " + this.access_token);
        }
        return yield this.soup.call(request, cancellable);
    }
}

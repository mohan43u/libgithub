/*
 * Soup - provides wrapper around libsoup
 *
 */

abstract class Github.SimpleSoupBase : Object {
    public HashTable<string, string> headers { get; set construct; }
    public ByteArray body { get; set construct; }
    
    construct {
        this.headers = this.headers ?? new HashTable<string, string>(str_hash, str_equal);
        this.body = this.body ?? new ByteArray();
    }
    
    public virtual void append_header(string key, string value) {
        this.headers.insert(key, value);
    }
    
    public virtual void append_string(string data) {
        this.body.append(data.data);
    }
    
    public virtual void append_bytes(Bytes data) {
        this.body.append(data.get_data());
    }
    
    public virtual string to_string() {
        this.body.data[this.body.len] = '\0';
        return (string) this.body.data;
    }
    
    public virtual Bytes to_bytes() {
        return new Bytes(this.body.data);
    }
}

class Github.SimpleSoupRequest : Github.SimpleSoupBase {
    public string method { get; set construct; }
    public string uri { get; set construct; }
    
    public SimpleSoupRequest() {
        Object();
    }
    
    public SimpleSoupRequest.props(string? method = null,
                                   string? uri = null,
                                   HashTable<string,string>? headers = null,
                                   ByteArray? body = null) {
        Object(method: method,
               uri: uri,
               headers: headers,
               body: body);
    }
}

class Github.SimpleSoupResponse : Github.SimpleSoupBase {
    public uint statuscode { get; set construct; }
    
    public SimpleSoupResponse() {
        Object();
    }
    
    public SimpleSoupResponse.props(uint? statuscode = 0,
                                    HashTable<string,string>? headers = null,
                                    ByteArray? body = null) {
        Object(statuscode: statuscode,
               headers: headers,
               body: body);
    }
}

class Github.SimpleSoup : Object {
    public string user_agent { get; set construct; }
    public uint chunk_size { get; set construct; }
    public string debug { get; set construct; }
    public signal void progress(double fraction);
    
    construct {
        this.user_agent = this.user_agent ?? "simplesoup.vala";
        this.chunk_size = this.chunk_size > 0 ? this.chunk_size : 4098;
        this.debug = this.debug ?? Environ.get_variable(Environ.get(), "SOUP_DEBUG");
    }
    
    public SimpleSoup() {
        Object();
    }
    
    public SimpleSoup.props(string? user_agent = null,
                            uint? chunk_size = 0,
                            string? debug = null) {
        Object(user_agent: user_agent,
               debug: debug);
    }
    
    private Soup.RequestHTTP setup(Github.SimpleSoupRequest request,
                                   out Github.SimpleSoupResponse response_out) throws Error {
        Github.SimpleSoupResponse response = new Github.SimpleSoupResponse();
        Soup.Session session = new Soup.Session();
        Soup.RequestHTTP souprequest = session.request_http(request.method, request.uri);
        Soup.Message message = souprequest.get_message();
        Soup.MessageHeaders headers = message.request_headers;

        if(this.debug != null) {
            HashTable<string, Soup.LoggerLogLevel> level = new HashTable<string, Soup.LoggerLogLevel>(str_hash, str_equal);
            level.insert("none", Soup.LoggerLogLevel.NONE);
            level.insert("minimal", Soup.LoggerLogLevel.MINIMAL);
            level.insert("headers", Soup.LoggerLogLevel.HEADERS);
            level.insert("body", Soup.LoggerLogLevel.BODY);
            this.debug = (this.debug in level ? this.debug : "none");
            session.add_feature(new Soup.Logger(level.get(this.debug), -1));
        }

        headers.append("User-Agent", this.user_agent);
        message.got_headers.connect(
            (message) => {
                response.statuscode = message.status_code;
                message.response_headers.foreach(response.append_header);
            });
        
        if(request.headers != null) {
            request.headers.foreach((key, value) => {
                    if(key == "Accept-Encoding" && (value == null || value.length <= 0)) {
                        session.remove_feature_by_type(typeof(Soup.ContentDecoder));
                        return;
                    }
                    headers.append(key, value);
                });
        }

        if(request.body.len > 0) message.request_body.append_take(request.body.data);
        response_out = response;
        return souprequest;
    }

    public async InputStream call_stream(Github.SimpleSoupRequest request,
                                         Cancellable? cancellable = null,
                                         out Github.SimpleSoupResponse response) throws Error {
        Idle.add(this.call_stream.callback);
        yield;
        Soup.RequestHTTP souprequest = this.setup(request, out response);
        this.progress(0.2);
        InputStream stream = yield souprequest.send_async(cancellable);
        this.progress(1);
        return stream;
    }
    
    public async Github.SimpleSoupResponse call(Github.SimpleSoupRequest request,
                                                Cancellable? cancellable = null) throws Error {
        Idle.add(this.call.callback);
        yield;
        Github.SimpleSoupResponse response = null;
        InputStream stream = yield this.call_stream(request, cancellable, out response);
        size_t part = 0;
        string content_length = response.headers.get("Content-Length");
        double whole = (content_length != null ? double.parse(content_length) : 0);
        this.progress(0.2);
        while(true) {
            Bytes chunk = yield stream.read_bytes_async(this.chunk_size, Priority.DEFAULT, cancellable);
            size_t chunksize = chunk.get_size();
            part += chunksize;
            if(whole > 0) this.progress(part/whole);
            response.append_bytes(chunk);
            if(chunksize <= 0) break;
        }
        this.progress(1);
        return response;
    }

    /*
    public static int main(string[] args) {
        if(args.length < 3) throw new Error(Quark.from_string("main()"), 1, "[usage] " + args[0] + " method url [outfile]");
        Github.SimpleSoupRequest request = new Github.SimpleSoupRequest.props(args[1], args[2]);
        Github.SimpleSoup soup = new Github.SimpleSoup();
        MainLoop mainloop = new MainLoop();

        soup.progress.connect((obj, fraction) => { print("progress: " + fraction.to_string() + "\n"); });
        soup.call.begin(
            request,
            null,
            (obj, res) => {
                try {
                    Github.SimpleSoupResponse response = soup.call.end(res);
                    if(args[3] != null) {
                        File outfile = File.new_for_path(args[3]);
                        FileOutputStream fout = outfile.create(FileCreateFlags.NONE);
                        fout.write_bytes_async.begin(response.to_bytes(),
                                                     Priority.DEFAULT,
                                                     null,
                                                     (obj, res) => {
                                                         try {
                                                             print("written: " + fout.write_bytes_async.end(res).to_string() + "\n");
                                                         }
                                                         catch (Error e) {
                                                             critical("[%d] %s", e.code, e.message);
                                                         }
                                                         mainloop.quit();
                                                     });
                    }
                    else {
                        print(response.to_string() + "\n");
                        mainloop.quit();
                    }
                }
                catch (Error e) {
                    critical("[%d] %s", e.code, e.message);
                    mainloop.quit();
                }
            });
        mainloop.run();
        return 0;
    }
    */
}
/*
 * channel.vala - communication channel between async functions and callbacks,
 *                inspired by Go's channels. There is no way in vala to make
 *                a signal callback started from a async function return to the same
 *                async function. this channel is a workaround to solve that problem.
 */


class Github.Channel : Object {
    private int[] fds;
    private InputStream sin;
    private OutputStream sout;
    
    construct {
        if(this.fds == null) {
            this.fds = new int[2];
            Posix.pipe(this.fds);
        }
        this.sin = new UnixInputStream(this.fds[0], true);
        this.sout = new UnixOutputStream(this.fds[1], true);
    }

    public Channel() {
        Object();
    }

    public void write(uint8[] buffer,
                      int io_priority = Priority.DEFAULT,
                      Cancellable? cancellable = null) throws Error {
        this.sout.write_async.begin(buffer, io_priority, cancellable);
    }

    public void write_bytes(Bytes buffer,
                            int io_priority = Priority.DEFAULT,
                            Cancellable? cancellable = null) throws Error {
        this.sout.write_bytes_async.begin(buffer, io_priority, cancellable);
    }

    public async Bytes read_bytes(Cancellable? cancellable = null) throws Error {
        Idle.add(this.read_bytes.callback);
        yield;
        ByteArray data = new ByteArray();
        while(true) {
            Bytes bytes = yield this.sin.read_bytes_async(4098, Priority.DEFAULT, cancellable);
            data.append(bytes.get_data());
            if((this.sin as PollableInputStream).is_readable() == false) break;
        }
        return ByteArray.free_to_bytes(data);
    }

    public async uint8[] read() throws Error {
        Bytes bytes = yield this.read_bytes();
        return Bytes.unref_to_data(bytes);
    }
}

/*
 * goaclient.vala - gnome-online-accounts client
 */

[DBus (name = "org.freedesktop.DBus.ObjectManager")]
interface Github.Goa : Object {
    public abstract async HashTable<string, HashTable<string, HashTable<string, Variant>>> GetManagedObjects() throws Error;
}
[DBus (name = "org.gnome.OnlineAccounts.Account")]
interface Github.GoaAccount : Object {
    public abstract string identity { owned get; }
    public abstract string ProviderName { owned get; }
    public abstract void EnsureCredentials(out int expires_in) throws Error;
    public abstract void Remove() throws Error;
}

[DBus (name = "org.gnome.OnlineAccounts.OAuth2Based")]
interface Github.GoaOAuth2 : Object {
    public abstract void GetAccessToken(out string access_token, out int expires_in) throws Error;
}

class Github.GoaClient : Object {
    public Github.Goa goa { get; set construct; }
    public Github.GoaAccount account { get; set construct; }
    public Github.GoaOAuth2 oauth2 { get; set construct; }
    public string dest { get; set construct; }
    public string path { get; set construct; }
    public string ProviderName { get; set construct; }
    public string access_token { get; set construct; }
    public int expires_in { get; set construct; }

    construct {
        this.dest = this.dest ?? "org.gnome.OnlineAccounts";
        this.path = this.path ?? "/org/gnome/OnlineAccounts";
        this.ProviderName = this.ProviderName ?? "Github";
    }

    public GoaClient() {
        Object();
    }

    public GoaClient.props(string? dest = null,
                           string? path = null,
                           string? ProviderName = null,
                           string? access_token = null,
                           int? expires_in = -1) {
        Object(dest: dest,
               path: path,
               ProviderName: ProviderName,
               access_token: access_token,
               expires_in: expires_in);
    }

    public async string? init(Cancellable? cancellable = null) throws Error {
        Idle.add(this.init.callback);
        yield;
        if(this.access_token != null) return this.access_token;
        this.goa  = yield Bus.get_proxy(BusType.SESSION, this.dest, this.path);
        if(this.goa == null) return null;
        this.path = null;
        HashTable<string, HashTable<string, HashTable<string, Variant>>> objects = yield this.goa.GetManagedObjects();
        foreach(string path in objects.get_keys()) {
            if(Regex.match_simple("/Accounts/", path) != true) continue;
            HashTable<string, HashTable<string, Variant>> account = objects.get(path);
            HashTable<string, Variant> props = account.get("org.gnome.OnlineAccounts.Account");
            if(props == null) continue;
            if(props.get("ProviderName").get_string() == this.ProviderName) {
                this.path = path;
                break;
            }
        }
        if(this.path == null) return null;
        this.account = yield Bus.get_proxy(BusType.SESSION, this.dest, this.path);
        this.oauth2 = yield Bus.get_proxy(BusType.SESSION, this.dest, this.path);
        string access_token = null;
        int expires_in = -1;
        this.oauth2.GetAccessToken(out access_token, out expires_in);
        this.access_token = access_token;
        this.expires_in = expires_in;
        return this.access_token ?? null;
    }

    /*
    public static int main(string[] args) {
        MainLoop mainloop = new MainLoop();
        Github.GoaClient client = new Github.GoaClient();
        client.init.begin(null, (obj, res) => {
                print("access_token: %s\n", client.init.end(res));
                print("identity: %s\n", client.access_token != null ? client.account.identity : null);
                print("ProviderName: %s\n", client.access_token != null ? client.account.ProviderName : null);
                mainloop.quit();
            });
        mainloop.run();
        return 0;
    }
    */
}
/*
 * secretstore.vala - wrapper to libsecret
 */

class Github.SecretStore : Object {
    public string app { get; set construct; }
    public HashTable<string, Secret.SchemaAttributeType> attributes { get; set construct; }
    public Secret.Schema schema { get; set construct; }

    construct {
        this.app = this.app ?? Environment.get_application_name();
        this.app = this.app ?? "secretstore.vala";
        if(this.attributes == null) {
            this.attributes = new HashTable<string, Secret.SchemaAttributeType>(str_hash, str_equal);
            this.attributes.insert("app", Secret.SchemaAttributeType.STRING);
            this.attributes.insert("key", Secret.SchemaAttributeType.STRING);
        }
        this.schema = this.schema ?? new Secret.Schema.newv("schema." + this.app,
                                                            Secret.SchemaFlags.NONE,
                                                            this.attributes);
    }

    public SecretStore() {
        Object();
    }

    public SecretStore.props(string? app = null,
                             HashTable<string, Secret.SchemaAttributeType>? attributes = null,
                             Secret.Schema? schema = null) {
        Object(app: app,
               attributes: attributes,
               schema: schema);
    }

    public async bool store(string key,
                            string value,
                            HashTable<string, string>? attrib = null,
                            Cancellable? cancellable = null) throws Error {
        Idle.add(this.store.callback);
        yield;
        HashTable<string, string> attributes = attrib;
        if(attributes == null) {
            attributes = new HashTable<string, string>(str_hash, str_equal);
            attributes.insert("app", this.app);
            attributes.insert("key", key);
        }
        return yield Secret.password_storev(this.schema, attributes, null, key, value, cancellable);
    }

    public async string retrive(string key,
                                HashTable<string, string>? attrib = null,
                                Cancellable? cancellable = null) throws Error {
        Idle.add(this.retrive.callback);
        yield;
        HashTable<string, string> attributes = attrib;
        if(attributes == null) {
            attributes = new HashTable<string, string>(str_hash, str_equal);
            attributes.insert("app", this.app);
            attributes.insert("key", key);
        }
        return yield Secret.password_lookupv(this.schema, attributes, cancellable);
    }

    public async bool remove(string key,
                             HashTable<string, string>? attrib = null,
                             Cancellable? cancellable = null) throws Error {
        Idle.add(this.remove.callback);
        yield;
        HashTable<string, string> attributes = attrib;
        if(attributes == null) {
            attributes = new HashTable<string, string>(str_hash, str_equal);
            attributes.insert("app", this.app);
            attributes.insert("key", key);
        }
        return yield Secret.password_clearv(this.schema, attributes, cancellable);
    }

    /*
    public static int main(string[] args) {
        if(args.length < 3) throw new Error(Quark.from_string("main()"), 1, "[usage] " + args[0] + " store|retrive|remove key [value]");
        Environment.set_prgname(Path.get_basename(args[0]));
        MainLoop mainloop = new MainLoop();
        Github.SecretStore store = new Github.SecretStore();
        switch(args[1]) {
        case "store":
            store.store.begin(args[2], args[3], null, null, (obj, res) => {
                    try {
                        store.store.end(res);
                    } catch(Error e) {
                        critical("[%d] %s\n", e.code, e.message);
                    }
                    mainloop.quit();
                });
            break;
        case "retrive":
            store.retrive.begin(args[2], null, null, (obj, res) => {
                    try {
                        print(store.retrive.end(res) + "\n");
                    } catch(Error e) {
                        critical("[%d] %s\n", e.code, e.message);
                    }
                    mainloop.quit();
                });
            break;
        case "remove":
            store.remove.begin(args[2], null, null, (obj, res) => {
                    try {
                        store.remove.end(res);
                    } catch(Error e) {
                        critical("[%d] %s\n", e.code, e.message);
                    }
                    mainloop.quit();
                });
            break;
        }
        mainloop.run();
        return 0;
    }
    */
}
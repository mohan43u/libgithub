/*
 * github.vala - implementing github api
 */

class Github.Client : Object, Github.OAuth2Authorizer {
    public Github.SimpleSoup soup { get; set construct; }
    public Github.SecretStore store { get; set construct; }
    public Github.GoaClient goaclient { get; set construct; }
    public Github.Webkit webkit { get; set construct; }
    public string client_id { get; set construct; }
    public string client_secret { get; set construct; }
    public string scope { get; set construct; }
    public string authorize_uri { get; set construct; }
    public string redirect_uri { get; set construct; }
    public string token_uri { get; set construct; }
    public string access_token { get; set construct; }

    construct {
        this.soup = this.soup ?? new Github.SimpleSoup();
        this.store = this.store ?? new Github.SecretStore();
        this.goaclient = this.goaclient ?? new Github.GoaClient();
        this.webkit = this.webkit ?? new Github.Webkit();
        this.client_id = this.client_id ?? "cc61935b01f65cf262a5";
        this.client_secret = this.client_secret ?? "40190a8926b434bd43f630b03113805f2a47d196";
        this.scope = this.scope ?? "gist";
        this.authorize_uri = this.authorize_uri ?? "https://github.com/login/oauth/authorize";
        this.redirect_uri = this.redirect_uri ?? "https://www.github.com/mohan43u/gistnotes";
        this.token_uri = this.token_uri ?? "https://github.com/login/oauth/access_token";
    }

    public Client() {
        Object();
    }

    public Client.props(Github.SimpleSoup? soup = null,
                        Github.SecretStore? store = null,
                        Github.GoaClient? goaclient = null,
                        Github.Webkit? webkit = null,
                        string? client_id = null,
                        string? client_secret = null,
                        string? scope = null,
                        string? authorize_uri = null,
                        string? redirect_uri = null,
                        string? token_uri = null,
                        string? access_token = null) {
        Object(soup: soup,
               store: store,
               goaclient: goaclient,
               webkit: webkit,
               client_id: client_id,
               client_secret: client_secret,
               scope: scope,
               authorize_uri: authorize_uri,
               redirect_uri: redirect_uri,
               token_uri: token_uri,
               access_token: access_token);
    }

    public static int main(string[] args) {
        if(args.length < 3) { printerr("[usage] %s method url [body]\n", args[0]); Posix.exit(1); }
        GLib.Environment.set_prgname(GLib.Path.get_basename(args[0]));
        MainLoop mainloop = new MainLoop();
        Github.Client github = new Github.Client();
        github.init.begin(args, null, (obj, res) => {
                try {
                    bool initdone = github.init.end(res);
                    if(initdone == true) {
                        Github.SimpleSoupRequest request = new Github.SimpleSoupRequest();
                        request.method = args[1];
                        request.uri = args[2];
                        if(args.length > 3) request.append_string(args[3]);
                        github.call.begin(request, null, (obj, res) => {
                                try {
                                    Github.SimpleSoupResponse response = github.call.end(res);
                                    print("statuscode: %u\n", response.statuscode);
                                    foreach(string header in response.headers.get_keys()) {
                                        print("%s : %s\n", header, response.headers.get(header));
                                    }
                                    print("body:\n%s\n", response.to_string());
                                } catch(Error e) {
                                    critical("[%d] %s", e.code, e.message);
                                }
                                mainloop.quit();
                            });
                    }
                    else {
                        mainloop.quit();
                    }
                } catch(Error e) {
                    critical("[%d] %s", e.code, e.message);
                    mainloop.quit();
                }
            });
        mainloop.run();
        return 0;
    }
}
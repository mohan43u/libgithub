/*
 * webkit.vala - wrap gtkwebkit for authorization purpose
 */

class Github.Webkit : Object {
    public WebKit.WebView webkit { get; set construct; }
    public string user_agent { get; set construct; }
    public string authorize_uri { get; set construct; }
    public string redirect_uri { get; set construct; }

    construct {
        this.user_agent = this.user_agent ?? "Mozilla/5.0 (GNOME; not Android) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile";
    }

    public Webkit() {
        Object();
    }

    public Webkit.props(WebKit.WebView? webkit = null,
                        string? user_agent = null,
                        string? authorize_uri = null,
                        string? redirect_uri = null) {
        Object(webkit: webkit,
               user_agent: user_agent,
               authorize_uri: authorize_uri,
               redirect_uri: redirect_uri);
    }

    public async string show(string[] args) throws Error{
        Idle.add(this.show.callback);
        yield;
        Gtk.init_check(ref args);
        Gtk.Window window = new Gtk.Window();
        Gtk.ScrolledWindow scroll = new Gtk.ScrolledWindow(null, null);
        Gdk.Screen screen = Gdk.Screen.get_default();
        Gdk.Geometry size = new Gdk.Geometry();
        this.webkit = new WebKit.WebView();
        WebKit.Settings settings = new WebKit.Settings();
        Github.Channel channel = new Github.Channel();

        window.set_title(Environment.get_application_name());
        size.min_width = (screen.get_width() * 25)/100;
        size.min_height = (screen.get_height() * 80)/100;
        window.set_geometry_hints(null, size, Gdk.WindowHints.MIN_SIZE);
        window.set_position(Gtk.WindowPosition.CENTER);
        window.delete_event.connect((event) => {
                try { channel.write("null".data); } catch(Error e) { critical("[%d] %s\n", e.code, e.message); }
                return true;
            });
        settings.user_agent = this.user_agent;
        this.webkit.set_settings(settings);
        this.webkit.load_changed.connect((event) => {
                if(event == WebKit.LoadEvent.REDIRECTED) {
                    string location = this.webkit.get_uri();
                    if(location.contains(this.redirect_uri)) {
                        window.close();
                        try { channel.write(location.data); } catch(Error e) { critical("[%d] %s\n", e.code, e.message); }
                    }
                }
            });
        this.webkit.load_uri(this.authorize_uri);
        scroll.add(this.webkit);
        window.add(scroll);
        window.show_all();
        uint8[] data = yield channel.read();
        data[data.length] = '\0';
        string redirected_uri = (string) data;
        return (redirected_uri == "null" ? null : redirected_uri);
    }

    /*
    public static int main(string[] args) {
        MainLoop mainloop = new MainLoop();
        Github.Webkit webkit = new Github.Webkit();
        webkit.redirect_uri = "https://www.github.com/mohan43u/gistnotes";
        webkit.authorize_uri = "https://github.com/login/oauth/authorize?client_id=cc61935b01f65cf262a5";
        webkit.authorize_uri += "&redirect_uri=" + Uri.escape_string("https://www.github.com/mohan43u/gistnotes");
        webkit.authorize_uri += "&scope=gist";
        webkit.show.begin(args, (obj, res) => {
                string? redirected_uri = webkit.show.end(res);
                print("%s\n", (redirected_uri == null ? "null" : redirected_uri));
                mainloop.quit();
            });
        mainloop.run();
        return 0;
    }
    */
}